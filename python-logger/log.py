import logging
from time import sleep

logging.basicConfig(format='1.0.1|%(asctime)s.%(msecs)03dZ|%(levelname)s|%(threadName)s|%(funcName)s|%(filename)s#%(lineno)d|%(message)s', level=logging.DEBUG)

count = 0
while 1:
    logging.debug('This is a debug message '+str(count))
    logging.info('This is a info message  '+str(count))
    logging.warning('This is a warning message  '+str(count))
    logging.error('This is a error message  '+str(count))
    count+=1
    sleep(1)