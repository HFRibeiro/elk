# ELK docker stack deployment for CREAM TEAM

### Requirements

* Make
* git
* Docker
* docker-compose


## Start 
```
make up
```

## Stop
```
make down
```

## Upload extra example logs
```
make logs_upload
```

### Injecting data

Open the Kibana web UI by opening <http://localhost:5601> in a web browser and use the following credentials to log in:

* user: *elastic*
* password: *CREAM_SKA*

Now that the stack is fully configured, you can go ahead and inject some log entries. The shipped Logstash configuration
allows you to send content via TCP:

```console
# Using BSD netcat (Debian, Ubuntu, MacOS system, ...)
$ cat /path/to/logfile.log | nc -q0 localhost 5000
```

```console
# Using GNU netcat (CentOS, Fedora, MacOS Homebrew, ...)
$ cat /path/to/logfile.log | nc -c localhost 5000
```

You can also load the sample data provided by your Kibana installation.

## curl example:

```
curl -XGET "http://elastic:CREAM_SKA@localhost:9200/_search" -H 'Content-Type: application/json' -d'
{
  "query": {
    "multi_match": {
        "query" : "log.py"
        , "fields": ["message"]
    }
  }
}'
```