up:
	docker-compose -f docker-compose.yml -f extensions/logspout/logspout-compose.yml \
	-f extensions/filebeat/filebeat-compose.yml up -d

down:
	docker-compose -f docker-compose.yml -f extensions/logspout/logspout-compose.yml \
	-f extensions/filebeat/filebeat-compose.yml down -v

logs_upload:
	cat logs_example.log | nc -q0 localhost 5000